<?php
require __DIR__ . '/__connect_db.php';
$result = [
    'success' => false, //資料取得是否成功
    'resultCode' => 400, //自訂狀態碼
    'errorMsg' => '沒有post資料', //錯誤訊息
    'postData' => [], //項目資料

];
if (!isset($_GET['sid'])) {
    $result['resultCode'] = 401;
    $result['errorMsg'] = '沒有sid';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit();

}
$sid = intval($_GET['sid']);
if (!empty($_POST['name']) and !empty($_POST['email'])) {
    try {
        $sql = "UPDATE `address_book` SET `name`=?,`email`=?,`mobile`=?,`address`=?,`birthday`=? WHERE `sid`=?";
        $stmt = $pdo->prepare($sql);  // 避免sql injection


        $stmt->execute([
            $_POST['name'],
            $_POST['email'],
            $_POST['mobile'],
            $_POST['address'],
            $_POST['birthday'],
            $sid
        ]);

        $r = $stmt->rowCount();

        if ($r== 1) {
            $info = [
                'type' => 'success',
                'text' => '資料修改成功'
            ];
        } elseif ($r == 0) {
            $info = [
                'type' => 'danger',
                'text' => '資料修改失敗'
            ];
        };

    } catch (PDOException $ex) {//__connect_db: setAttribute
        echo $ex->getMessage();
//        echo '---' . $ex->getCode();
//        $info = [
//            'type' => 'danger',
//            'text' => 'email 請勿重複'
//        ];
    }


}
echo json_encode($result, JSON_UNESCAPED_UNICODE);
