<?php
//require __DIR__ . '/__connect_db.php';
$pageName = 'add2';

?>
<?php include __DIR__ . '/__html_head.php'; ?>
<?php include __DIR__ . '/__navbar.php'; ?>

    <div class="container mt-4">

        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <?php if (isset($info)): ?>
                    <div class="alert alert-<?= $info['type'] ?>" role="alert">
                        <?= $info['text'] ?>
                    </div>
                <?php endif; ?>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">新增資料</h5>
                        <form method="post" onsubmit="return formCheck()">
                            <div class="form-group">
                                <label for="exampleInputEmail1">姓名</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter email">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       placeholder="Enter mobile">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">生日</label>
                                <input type="date" class="form-control" id="birthday" name="birthday"
                                       placeholder="Enter birthday">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">地址</label>
                                <input type="text" class="form-control" id="address" name="address"
                                       placeholder="Enter address">

                            </div>
                            <button type="submit" class="btn btn-primary">新增</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // 全域變數，不要使用name當變數名稱
        var i_name = $('#name'),
            i_email = $('#email'),
            i;


        function formCheck() {
            var birthday_pattern = /\d{4}\-\d{1,2}\-\d{1,2}/;
            var isPass = true;
            if (!i_name.val()) {
                alert('請填寫姓名');
                isPass = false;
            }
            if (!i_email.val()) {
                alert('請填寫email');
                isPass = false;
            }

            if(isPass){
                fetch('ab_add_api.php',{
                    method:"POST",
                    mode:"cors",
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                    body: $(document.forms[0]).serialize()
                }).then(function(resp){
                    return resp.json();//如果要除錯就用text()
                }).then(function(json){//如果要除錯就用(txt)
                    console.log(json);//如果要除錯就用console.log(txt)
                }).catch(function(ex){
                    console.log('錯誤:', ex);
                })

            }
            return false;
        }

    </script>


<?php include __DIR__ . '/__html_footer.php'; ?>