<?php
require __DIR__ . '/__connect_db.php';
$pageName = 'edit';
if (!isset($_GET['sid'])) {

    exit();

}
$sid = intval($_GET['sid']);
if (!empty($_POST['name']) and !empty($_POST['email'])) {
    try {
        $sql = "UPDATE `address_book` SET `name`=?,`email`=?,`mobile`=?,`address`=?,`birthday`=? WHERE `sid`=?";
        $stmt = $pdo->prepare($sql);  // 避免sql injection


        $stmt->execute([
            $_POST['name'],
            $_POST['email'],
            $_POST['mobile'],
            $_POST['address'],
            $_POST['birthday'],
            $sid
        ]);

        $result = $stmt->rowCount();

        if ($result == 1) {
            $info = [
                'type' => 'success',
                'text' => '資料修改成功'
            ];
        } elseif ($result == 0) {
            $info = [
                'type' => 'danger',
                'text' => '資料修改失敗'
            ];
        };

    } catch (PDOException $ex) {
        echo $ex->getMessage();
//        echo '---' . $ex->getCode();
//        $info = [
//            'type' => 'danger',
//            'text' => 'email 請勿重複'
//        ];
    }


}
$r_sql = "SELECT * FROM address_book WHERE sid=$sid";
$r_row = $pdo->query($r_sql)->fetch(PDO::FETCH_ASSOC);

if (empty($r_row)) {
    header('Location: ab_list.php');
    exit;
}

?>
<?php include __DIR__ . '/__html_head.php'; ?>
<?php include __DIR__ . '/__navbar.php'; ?>

    <div class="container mt-4">

        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <?php if (isset($info)): ?>
                    <div class="alert alert-<?= $info['type'] ?>" role="alert">
                        <?= $info['text'] ?>
                    </div>
                <?php endif; ?>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">修改資料</h5>
                        <form method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">姓名</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       value="<?= htmlentities($r_row['name']) ?>" placeholder=" Enter name">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" readonly
<!--                                       disabled就不會送此欄，readonly會送-->
                                       value="<?= htmlentities($r_row['email']) ?>"
                                       placeholder=" Enter email">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       value="<?= htmlentities($r_row['mobile']) ?>"
                                       placeholder=" Enter mobile">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">生日</label>
                                <input type="date" class="form-control" id="birthday" name="birthday"
                                       value="<?= htmlentities($r_row['birthday']) ?>"
                                       placeholder=" Enter birthday">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">地址</label>
                                <input type="text" class="form-control" id="address" name="address"
                                       value="<?= htmlentities($r_row['address']) ?>"
                                       placeholder=" Enter address">

                            </div>
                            <button type="submit" class="btn btn-primary">修改</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var name = $('#name'),
            email = $('#email');
        i;

        function formCheck() {
            var birthday_pattern = /\d{4}\-\d{1,2}\-\d{1,2}/;
            var isPass = true;
            if (!name.val()) {
                alert('請填寫姓名');
                isPass = false;
            }
            if (!email.val()) {
                alert('請填寫email');
                isPass = false;
            }
            return isPass;
        }

    </script>


<?php include __DIR__ . '/__html_footer.php'; ?>