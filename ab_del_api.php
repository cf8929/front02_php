<?php
require __DIR__ . '/__connect_db.php';

$result = array(
    'success' => false,
    'resultCode' => 400,
    'errorMsg' => '沒有sid參數',
    'rowCount' => 0
);


if (!isset($_GET['sid'])) {
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit();
}
$sid = intval($_GET['sid']);

$sql = "DELETE FROM `address_book` WHERE sid=$sid";

$stmt = $pdo->query($sql);

if ($stmt->rowCount() == 1) {
    $result = array(
        'success' => true,
        'resultCode' => 200,
        'errorMsg' => '',
        'rowCount' => 1
    );
} else {
    $result['resultCode'] = 402;
    $result['errorMsg'] = '資料沒有刪除';
}
echo json_encode($result, JSON_UNESCAPED_UNICODE);
//echo $stmt->rowCount();

//header('Location:ab_list.php'); //回到列表頁

//if(isset($_SERVER['HTTP_REFERER'])){
//    header('Location:'. $_SERVER['HTTP_REFERER']);
//}else{
//    header('Location: ab_list.php');
//}
