<?php
require __DIR__ . '/__connect_db.php';
try {
    $stmt = $pdo->query("SELECT `email`, `name` FROM address_book");
//    $stmt = $pdo->query("SELECT 2+3"); 測試是否有連到mysql，讓主機做簡單運算

//    $data_ar = $stmt->fetchAll();// 索引值和關聯式兩種都給

//    $data_ar = $stmt -> fetchAll(PDO::FETCH_ASSOC);  //只給關聯式的陣列
    // 在下條件之後再用fetchAll
    $data_ar = $stmt -> fetch(PDO::FETCH_ASSOC);  //一筆一筆拿
    echo json_encode($data_ar, JSON_UNESCAPED_UNICODE);
//    print_r($data_ar);
    $data_ar = $stmt -> fetch(PDO::FETCH_ASSOC);  //一筆一筆拿
    echo json_encode($data_ar, JSON_UNESCAPED_UNICODE);
} catch (PDOException $ex) {
    echo 'Connection failed:' . $ex->getMessage();
}