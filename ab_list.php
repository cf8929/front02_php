<?php
require __DIR__ . '/__connect_db.php';
$pageName = 'list';
$per_page = 5;//每頁有幾筆
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;  //當沒有設定?page=的時候回傳第一頁
// 當刪除的頁面剩下一個欄位，刪除後就會跳轉到空頁

$t_sql = "SELECT COUNT(1) FROM address_book";
$total_rows = $pdo->query($t_sql)->fetch()[0];
$total_pages = ceil($total_rows / $per_page);

if($page<1){
    header('Location: ab_list.php');
    exit;
};
if($page>$total_pages){
    header('Location:ab_list.php?page='.$total_pages);
};


$sql = sprintf("SELECT * FROM address_book ORDER BY sid DESC LIMIT %s, %s", ($page - 1) * $per_page, $per_page);
$stmt = $pdo->query($sql);



?>
<?php include __DIR__ . '/__html_head.php'; ?>
<?php include __DIR__ . '/__navbar.php'; ?>

    <div class="container mt-4">
<!--        <div>--><?//= $total_rows . '::' . $total_pages ?><!--</div>-->
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item <?= $page==1 ? 'disabled' : ''?>"><a class="page-link" href="?page=<?=$page-1?>">Previous</a></li>
                <?php for($i=1; $i<=$total_pages; $i++):?>
                    <li class="page-item <?= $i==$page ? 'active' : ''?>">
                        <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
                <?php endfor ?>

                <li class="page-item <?= $page==$total_pages ? 'disabled' : ''?>"><a class="page-link" href="?page=<?=$page+1?>"">Next</a></li>
            </ul>
        </nav>
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">email</th>
                <th scope="col">mobile</th>
                <th scope="col">address</th>
                <th scope="col">birthday</th>
                <th scope="col">DEL</th>
                <th scope="col">EDIT</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($r = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

                <tr>
                    <th scope="row"><?= $r['sid'] ?></th>
                    <th><?= $r['name'] ?></th>
                    <th><?= $r['email'] ?></th>
                    <th><?= $r['mobile'] ?></th>
                    <th><?= $r['address'] ?></th>
                    <th><?= $r['birthday'] ?></th>
                    <th><a href="javascript:del_it(<?= $r['sid'] ?>)"><i class="fas fa-trash-alt"></i></a></th>
                    <th><a href="ab_edit.php?sid=<?= $r['sid'] ?>"><i class="fas fa-edit"></i></a></th>

                </tr>
            <?php endwhile; ?>


            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item <?= $page==1 ? 'disabled' : '' ?>"><a class="page-link" href="?page=1">&lt;&lt;</a></li>
                <li class="page-item <?= $page==1 ? 'disabled' : '' ?>"><a class="page-link" href="?page=<?=$page-1?>">&lt;</a></li>
                <li class="page-item"><a class="page-link"><?=$page.'/'.$total_pages?></a></li>
                <li class="page-item <?= $page==$total_pages ? 'disabled' : '' ?>"><a class="page-link" href="?page=<?=$page+1?>">&gt;</a></li>
                <li class="page-item <?= $page==$total_pages ? 'disabled' : '' ?>"><a class="page-link" href="?page=<?=$total_pages?>">&gt;&gt;</a></li>
            </ul>
        </nav>
    </div>
    <script>
        function del_it(sid){
            if(confirm('你確定要刪除編號為'+sid+'的資料嗎?')){
                location.href='ab_del.php?sid='+ sid;
            }
        }

    </script>

<?php include __DIR__ . '/__html_footer.php'; ?>