<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border="1">

    <?php for ($j = 1; $j <= 10; $j++): ?>
        <tr>
            <?php for ($i = 1; $i <= 10; $i++):
                // echo '<td>'. $i * $j .'</td>';
                printf('<td>%s*%s=%x</td>', $i,$j,$i*$j);  //phpstorm會看到format:的提示字，實際不存在於程式碼內
            endfor ?>
        </tr>
    <?php endfor ?>
</table>

</body>
</html>