<?php

$str = '0918-222-333';

// 手機號碼規則
$pattern = '/^09\d{2}\-?\d{3}\-?\d{3}$/';

// 符合回傳1,無符合回傳0
echo preg_match($pattern, $str) ? 'right' : 'wrong';