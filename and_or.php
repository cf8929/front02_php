<?php

$a = 2 or $b = 3;  //$a=2 已經是TRUE故後面略過

echo "\$a:$a, \$b:$b <br>";

$c = 2 || $d = 3; //$c=(2||$d=3) 2已經是TRUE故後面略過，丟出TRUE

echo "\$c:$c, \$d:$d <br>";  //$c:1  (true)