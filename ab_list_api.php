<?php
require __DIR__ . '/__connect_db.php';
$per_page = 5;//每頁有幾筆
$result = [
    'success' => false, //資料取得是否成功
    'resultCode' => 400, //自訂狀態碼
    'errorMsg' => '', //錯誤訊息
    'perPage' => $per_page, //每頁有五筆
    'data' => [], //項目資料


];

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;  //當沒有設定?page=的時候回傳第一頁
// 當刪除的頁面剩下一個欄位，刪除後就會跳轉到空頁

$t_sql = "SELECT COUNT(1) FROM address_book";
$total_rows = $pdo->query($t_sql)->fetch()[0];
$total_pages = ceil($total_rows / $per_page);

$result['totalRows'] = $total_rows; //給總共筆數
$result['totalPages'] = $total_pages; //給共有幾頁

if ($page < 1) {
    $result['resultCode'] = 402;
    $result['errorMsg'] = '頁碼值小於一';
    echo json_encode($result,JSON_UNESCAPED_UNICODE);
    exit;
};
if ($page > $total_pages) {
    $result['resultCode'] = 403;
    $result['errorMsg'] = '頁碼值大於最大頁數';
    echo json_encode($result,JSON_UNESCAPED_UNICODE);
    exit;
};

$result['page'] = $page; //給第幾頁


$sql = sprintf("SELECT * FROM address_book ORDER BY sid DESC LIMIT %s, %s", ($page - 1) * $per_page, $per_page);
$result['data'] = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$result['success'] = true;
$result['resultCode'] = 200;

echo json_encode($result, JSON_UNESCAPED_UNICODE);