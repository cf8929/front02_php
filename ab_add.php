<?php
require __DIR__ . '/__connect_db.php';
$pageName = 'add';

if (!empty($_POST['name']) and !empty($_POST['email'])) {
//        $sql = sprintf("INSERT INTO `address_book`(`name`, `email`, `mobile`, `address`, `birthday`, `created_at`) VALUES (%s,%s,%s,%s,%s)",
//            $pdo->quote($_POST['name']),
//            $pdo->quote($_POST['email']),
//            $pdo->quote($_POST['mobile']),
//            $pdo->quote($_POST['address']),
//            $pdo->quote($_POST['birthday'])
//
//            );
//        echo $sql;

    try {
        $sql = "INSERT INTO `address_book`(`name`, `email`, `mobile`, `address`, `birthday`, `created_at`) VALUES (?,?,?,?,?,NOW())";
        $stmt = $pdo->prepare($sql);  // 避免sql injection

//    echo $pdo->errorCode().'<br>'.'---<br>';
//    echo $stmt->errorCode().'<br>'.'---<br>';
//    print_r($pdo->errorInfo());
//    print_r($stmt->errorInfo());

        $stmt->execute([
            $_POST['name'],
            $_POST['email'],
            $_POST['mobile'],
            $_POST['address'],
            $_POST['birthday']
        ]);

        $result = $stmt->rowCount();

        if ($result == 1) {
//        echo"<script>alert('資料新增完成')</script>";
            $info = [
                'type' => 'success',
                'text' => '資料新增完成'
            ];
        } elseif ($result == 0) {
            $info = [
                'type' => 'danger',
                'text' => '資料新增失敗'
            ];
        };

    } catch (PDOException $ex) {
        echo $ex->getMessage();
        echo '---'.$ex->getCode();
        $info = [
            'type' => 'danger',
            'text' => 'email 請勿重複'
        ];
    }


}

?>
<?php include __DIR__ . '/__html_head.php'; ?>
<?php include __DIR__ . '/__navbar.php'; ?>

    <div class="container mt-4">

        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <?php if (isset($info)): ?>
                    <div class="alert alert-<?= $info['type'] ?>" role="alert">
                        <?= $info['text'] ?>
                    </div>
                <?php endif; ?>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">新增資料 <?php isset($result) ? var_dump($result) : '' ?></h5>
                        <form method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">姓名</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter email">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       placeholder="Enter mobile">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">生日</label>
                                <input type="date" class="form-control" id="birthday" name="birthday"
                                       placeholder="Enter birthday">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">地址</label>
                                <input type="text" class="form-control" id="address" name="address"
                                       placeholder="Enter address">

                            </div>
                            <button type="submit" class="btn btn-primary">新增</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var name = $('#name'),
            email = $('#email');
        i;

        function formCheck() {
            var birthday_pattern = /\d{4}\-\d{1,2}\-\d{1,2}/;
            var isPass = true;
            if (!name.val()) {
                alert('請填寫姓名');
                isPass = false;
            }
            if (!email.val()) {
                alert('請填寫email');
                isPass = false;
            }
            return isPass;
        }

    </script>


<?php include __DIR__ . '/__html_footer.php'; ?>